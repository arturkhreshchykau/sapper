﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Bomb
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int Damage { get; set; }
    }
}
