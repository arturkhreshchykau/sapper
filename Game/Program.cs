﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Sapper
    {
        readonly int heigth = 10;
        readonly int width = 10;
        int health = 10;
        readonly int bombs_number = 10;
        int x = 2;
        int y = 2;
        bool bombed = false;
        readonly Random random = new Random();

        private void WriteBoard(int x, int y)
        {
            Console.Clear();
            for (int i = 1; i <= width; i++)
            {
                Console.SetCursorPosition(i + 1, 1);
                Console.Write("-");
            }
            for (int i = 1; i <= heigth; i++)
            {
                Console.SetCursorPosition(1, i + 1);
                Console.Write("+");
            }
            for (int i = 1; i <= width; i++)
            {
                Console.SetCursorPosition(i + 1, heigth + 2);
                Console.Write("-");
            }
            for (int i = 1; i <= heigth; i++)
            {
                Console.SetCursorPosition(width + 2, i + 1);
                Console.Write("+");
            }

            Moving(x, y);
        }

        private List<Bomb> WriteBombs()
        {
            List<Bomb> bombs = new List<Bomb>();
            for (int i = 0; i < bombs_number; i++)
            {
                Bomb bomb = new Bomb
                {
                    PositionX = random.Next(3, 10),
                    PositionY = random.Next(3, 10),
                    Damage = random.Next(1, 10)
                };
                bombs.Add(bomb);
            }

            return bombs;
        }

        private void Moving(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.Write("&");
        }

        private void Logic(List<Bomb> bombs)
        {
            ConsoleKeyInfo key = Console.ReadKey(true);
            Console.SetCursorPosition(x, y);
            int xOld = x;
            int yOld = y;

            if (bombed)
            {
                Console.Write("X");
                bombed = false;
            }
            else
            {
                Console.Write(" ");
            }
            
            switch (key.KeyChar)
            {
                case 'w':
                    y--;
                    break;
                case 's':
                    y++;
                    break;
                case 'd':
                    x++;
                    break;
                case 'a':
                    x--;
                    break;
            }

            foreach (var bomb in bombs)
            {
                if (bomb.PositionX == x && bomb.PositionY == y)
                {
                    health -= bomb.Damage;
                    bombed = true;
                    Warning(health, x, y);
                }
            }

            if (x > 11 || x < 2 || y > 11 || y < 2)
            {
                Console.SetCursorPosition(xOld, yOld);
                Console.Write("&");
                x = xOld;
                y = yOld;
            }
            else if (x == 11 && y == 11)
            {
                Console.SetCursorPosition(15, 15);
                DisplayingWarning("You won!!! You can try again! \n\t\t\t\t Press Enter to continue");
                Main();
            }
            else
            {
                Moving(x, y);
            }
        }

        private void Warning(int health, int x, int y)
        {
            Console.SetCursorPosition(15, 15);
            string text;
            if (health <= 0)
            {
                text = "There was a bomb! You're dead! You can try again! \n\t\t\t\t Press Enter to continue";
                DisplayingWarning(text);
                Main();
            }
            else
            {
                text = "There was a bomb! You have " + health + " health points out of 10. \n\t\t\t\t Press Enter to continue!";
                DisplayingWarning(text);
                WriteBoard(x, y);
            }
        }

        private void DisplayingWarning(string text)
        {
            Console.WriteLine(text);
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);
            } while (key.Key != ConsoleKey.Enter);
        }

        static void Main()
        {
            Console.Clear();
            Sapper sapper = new Sapper();
            sapper.DisplayingWarning("Welcome. You need to get to the bottom right corner! \n Use 'w', 's', 'a', 'd'! \n For start press Enter!");
            Console.CursorVisible = false;
            sapper.WriteBoard(2, 2);
            List<Bomb> bombs = sapper.WriteBombs();
            do
            {
                sapper.Logic(bombs);
            } while (true);
        }
    }
}
